const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const auth = require('../auth');

//to check for existing email

router.post('/email-exists', (req, res)=>{
	UserController.emailExists(req.body).then(result => res.send(result))
})

//Register a user 

router.post('/', (req, res)=>{
	UserController.register(req.body).then(result => res.send(result))
});


//login a user
router.post('/login', (req,res)=>{
	UserController.login(req.body).then(result => res.send(result))
})

//routes for getting the details of a user
router.get('/details', auth.verify, (req, res)=>{
	const user = auth.decode(req.headers.authorization)
	UserController.get({ userId: user.id}).then(user => res.send(user))
})
//routes for updating user to admin 
router.put("/:id/add-admin", auth.verify, (req,res) =>{
	UserController.setAsAdmin(req.params.id, req.body).then(setAsAdmin => res.send(setAsAdmin))

})
//retrieve all user's orders
router.get("/:id/retrieveorders", auth.verify, (req,res) =>{
	const user = auth.decode(req.heads.authorization).id === req.params.id
	const admin = auth.decode(req.heads.authorization).isAdmin === false
	if(user){

		if (admin){
			return UserController.retrieveOrders(req.params.id, req.body).then(result => res.send(result))
			}else{
				return res.send("You are in admin's account can't retrieve user's order")
			}
	}else{
			return res.send("You are not login to your account")
		}

})
//retrieve all orders in admin's account 
// router.get("/retrieveallorders", auth.verify, (req,res) =>{
// 	const admin = auth.decode(req.heads.authorization).isAdmin === true

// 		if (admin){
// 			return UserController.retrieveAllOrders(req.params.id, req.body).then(result => res.send(result))
// 			}else{
// 				return res.send("Can't retrieve orders , login to admin's account!")
// 			}
	
			
	


// router.post('/order', auth.verify, (req, res)=>{
// 	let data ={
// 		userId: auth.decode(req.headers.authorization).id, 
// 		quantity:req.body.quantity,
// 		productId: req.body.productId,
// 		price:req.body.price,
// 		totalAmount:req.body.totalAmount
// 	}
// 	UserController.order(data).then(result => res.send(result))
// })


	

module.exports = router