const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productController');
const auth = require('../auth');


//create a product
router.post("/product-added", auth.verify, (req, res) => {

	ProductController.addProduct(req.body).then(addedProduct => res.send(addedProduct));

	
});
//create a product if admin else failed
router.post("/:id/product-addedifadmin",(req,res)=>{
	ProductController.addProductAdmin(req.params.id, req.body).then(create=>res.send(create))
})
router.get('/activeproducts', (req, res)=>{
	ProductController.get({ isActive: true}).then(getproduct => res.send(getproduct))
})

router.get("/:id", (req,res) =>{
	ProductController.getProduct(req.params.id).then(
		singleproduct => res.send(singleproduct));
})
//Update product with no admin accesss
router.put("/:id", auth.verify, (req,res) =>{
	ProductController.updateProduct(req.params.id, req.body).then(
		updateProduct=> res.send(updateProduct));
})
//Update product with admin access
router.put("/:id/update", auth.verify, (req,res) =>{
	const user = auth.decode(req.heads.authorization).isAdmin === true
	if(user){
		ProductController.updateProduct(req.params.id, req.body).then(result => res.send(result))
		}else{
			return `Not Admin`
		}
})

	
router.put("/:id/archive", auth.verify, (req, res)=>{
	ProductController.archiveProduct(req.params.id).then(updateProduct=> res.send(updateProduct))
})
router.put("/:id/archiveadmin", auth.verify, (req,res) =>{
	const user = auth.decode(req.heads.authorization).isAdmin === true
	if(user){
		ProductController.archiveProductAdmin(req.params.id, req.body).then(result => res.send(result))
		}else{
			return `Not Admin`
		}
})




module.exports = router