//Setup the dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');//This allows your sites to connect to this server
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');

//database connection
mongoose.connect('mongodb+srv://admin:admin@b106.m9tdb.mongodb.net/capstone2',{
	useNewUrlParser: true, //to use the new parser 
	useUnifiedTopology: true, // to use the new server discover and monitoring engine
	useFindAndModify: false
})

mongoose.connection.once('open', () => console.log('Now Connected to Database'))

//server setup
const app = express();

//it allows resources sharing from all origins
app.use(cors())//Tells the server that cors is being used by your server
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//routes
app.use("/products", productRoutes);
app.use("/users", userRoutes)


app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
});

//process.env.PORT is a port that can be assigned by your hosting service
//
