const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {``
		type: String,
		required: [true, 'First name is required']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},

	email: {
		type: String,
		required: [true, 'Email is required']
	},

	password: {
		type: String,
		required: [true, 'Password is required']
	},

	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	isActive: {
		type: Boolean,
		default: true
	},

	order: [
		{
			productId: {
				type: String,
				required: [true, 'Product Id is required']
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Registered' //Alternative values are 'Cancelled' and 'Completed'
			},
			quantity:{
				type: Number,
				required: [true, 'quantity is required'],
				default: 1
			},
		
			totalAmount:{
				type: Number
				
			}
		}

	]



})

module.exports = mongoose.model('User', userSchema)
