const mongoose = require('mongoose');
const auth = require('../auth');
const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required:[true, 'Product name is required']
	},

	description: {
		type: String,
		required: [true, 'Description is required']
	},

	price: {
		type: Number,
		required: [true, 'Price is required']
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	order: [{
		userId: {
			type: String,
			required: [true, 'User Id is required']
		},
		orderedOn:{
			type: Date,
			default: new Date()
		}
	}

	]
	
})

module.exports = mongoose.model('Products', productSchema)