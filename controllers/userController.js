const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.emailExists = (body) => {
	return User.find({ email: body.email }).then(result => {
		return result.length > 0 ? true : false
	})
}



module.exports.register = (params) =>{
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//10 = salt/string of characters added to password before hashing
	})


	return user.save().then((user, err)=>{
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user =>{
		if(user === null){
			return false
		}

		//compares password received and hashed password
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

//the mongoose toObject method converts the mongoose object into a plain js object
		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}

	
	})
}

//3 -set as USER TO ADMIN
module.exports.setAsAdmin = (userId, body)=>{
	newAdmin = {
		isAdmin: true
		
	}

	return User.findByIdAndUpdate(userId, newAdmin,(err, success) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{

			return true;
		}

	})
}

//module.exports 
module.exports.get =(params)=>{
	return User.findById(params.userId).then(user=>{
		//re-assign the apssword to undefined so it won't retrieved along with th user data
		user.password = undefined 
		return user
	})
}
//retrieve user - no admin access 
module.exports.retrieveOrders = (params) => {
	return User.findById(params.id).then(users =>{
		return User.findbyID(params).then(orders => {
			pending = orders.orderRequest
			return pending
		})
	})
}
retrieve order in admin account 
module.exports.retrieveAllOrders = (params) => {
		return User.find(params).then(orders => {
			var allorders = []
			for(var i=0; i<orders.length; ++i){
				allorders.push(orders[i]["orders"])	

			}
			return allorders

		})
	



// module.exports.enroll = (params) =>{
// 	return User.findById(params.userId).then(user =>{
// 		user.enrollments.push({courseId: params.courseId})

// 		return user.save().then((user, err)=>{
// 			return Course.findById(params.courseId).then(course =>{
// 				course.enrollees.push({userId: params.userId})

// 				return course.save().then((course, err)=>{
// 					return (err) ? false : true
// 				})
// 			})
// 		})
// 	})
// }

Async and await - allow process to wait for each other
it improves readability of the code
Async await brings to the tables is that you can explicitly tell your program for the evaluation of a certain statement before proceeding 

module.exports.order = async (data) =>{

	
	let userSaveStatus = await User.findById(data.userId).then(user =>{
			user.order.push({
				quantity:data.quantity,
				productId:data.productId,
				price:data.price,
				totalAmount:data.totalAmount
	});
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true

			}
		})
	})
	
	let productSaveStatus = await Product.findById(data.productId).then(product =>{
		product.users.push({userId: data.userId});
	
	return product.save().then((product, error)=>{
		if(error){
			return false;
		}else{
			return true
		}
});
		
// 	})//the courseSaveStatus tells us if the user was successfully saved to the course
// 	//We have to check if the courseSaveStatus and userSaveStatus are both successful

	if(userSaveStatus && productSaveStatus){
		return true;

	}else{
		return false
	}
}